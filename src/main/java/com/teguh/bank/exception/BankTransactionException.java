package com.teguh.bank.exception;

public class BankTransactionException extends Exception {

    public BankTransactionException(String message) {
        super(message);
    }

}