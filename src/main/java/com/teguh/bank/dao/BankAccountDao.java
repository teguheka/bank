package com.teguh.bank.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.teguh.bank.dto.BankAccountInfo;
import com.teguh.bank.entity.BankAccount;
import com.teguh.bank.exception.BankTransactionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

//reference: https://stackoverflow.com/questions/8490852/spring-transactional-isolation-propagation
@Repository
public class BankAccountDao {
    @Autowired
    private EntityManager entityManager;


    public BankAccountDao() {
    }

    public BankAccount findById(Long id) {
        return this.entityManager.find(BankAccount.class, id);
    }

    public List<BankAccountInfo> listBankAccountInfo() {
        String sql = "SELECT new " + BankAccountInfo.class.getName() + "(e.id,e.name,e.balance) from " + BankAccount.class.getName() + " e ORDER BY e.id";
        Query query = entityManager.createQuery(sql, BankAccountInfo.class);
        return query.getResultList();
    }

    // MANDATORY: Transaction must be created before. method must run within a transaction. If no existing transaction is in progress, an exception will be thrown
    @Transactional(propagation = Propagation.MANDATORY)
    public void addAmount(Long id, double amount) throws BankTransactionException {
        BankAccount account = this.findById(id);
        if (account == null) {
            throw new BankTransactionException("Account not found " + id);
        }
        double newBalance = account.getBalance() + amount;
        if (newBalance < 0) {
            throw new BankTransactionException(String.format("The money in the account '%d' is not enough (current balance %.2f)", id, account.getBalance()));
        }
        account.setBalance(newBalance);
    }

    // Propagation.REQUIRES_NEW = If DataSourceTransactionObject T1 is already started for Method M1 and it is in progress(executing method M1).
    // If another method M2 start executing then T1 is suspended for the duration of method M2 with new DataSourceTransactionObject T2 for M2.
    // M2 run within its own transaction context
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = BankTransactionException.class)
    public void sendMoney(Long fromAccountId, Long toAccountId, double amount) throws BankTransactionException {
        addAmount(toAccountId, amount);
        addAmount(fromAccountId, -amount);
    }
}
