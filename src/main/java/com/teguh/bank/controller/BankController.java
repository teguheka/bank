package com.teguh.bank.controller;

import com.teguh.bank.dao.BankAccountDao;
import com.teguh.bank.dto.BankAccountInfo;
import com.teguh.bank.exception.BankTransactionException;
import com.teguh.bank.form.SendMoneyForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class BankController {
    @Autowired
    private BankAccountDao bankAccountDao;

    @GetMapping(value = "/")
    public String showBankAccounts(Model model) {
        List<BankAccountInfo> list = bankAccountDao.listBankAccountInfo();
        model.addAttribute("accountInfos", list);
        return "accounts-page";
    }

    @GetMapping(value = "/send-money")
    public String viewSendMoneyPage(Model model) {
        List<BankAccountInfo> list = bankAccountDao.listBankAccountInfo();
        SendMoneyForm form = new SendMoneyForm(1L, 2L, 700d);

        model.addAttribute("accountInfos", list);
        model.addAttribute("sendMoneyForm", form);
        return "send-money-page";
    }


    @PostMapping(value = "/send-money")
    public String processSendMoney(Model model, SendMoneyForm sendMoneyForm) {

        System.out.println("Send Money: " + sendMoneyForm.getAmount());

        List<BankAccountInfo> list = bankAccountDao.listBankAccountInfo();

        try {
            bankAccountDao.sendMoney(
                    sendMoneyForm.getFromAccountId(),
                    sendMoneyForm.getToAccountId(),
                    sendMoneyForm.getAmount()
            );
        } catch (BankTransactionException e) {
            model.addAttribute("accountInfos", list);
            model.addAttribute("errorMessage", "Error: " + e.getMessage());
            return "/send-money-page";
        }
        return "redirect:/";
    }
}
