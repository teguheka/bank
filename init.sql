-- postgres
CREATE TABLE "bank_account" (
  "name" varchar(128) NOT NULL,
  "balance" float4 NOT NULL,
  "id" int4 NOT NULL DEFAULT nextval('bank_account_id_seq'::regclass),
  CONSTRAINT "bank_account_pkey" PRIMARY KEY ("id")
);

--mysql
create table BANK_ACCOUNT
(
  id  bigint(20) NOT NULL AUTO_INCREMENT,
  name VARCHAR(128) not null,
  balance DOUBLE not null
) ;
--
alter table bank_accountadd constraint bank_account_pk primary key (id);


Insert into bank_account(id, name, Balance) values (1, 'Tom', 1000);
Insert into bank_account(id, name, Balance) values (2, 'Jerry', 2000);
Insert into bank_account(id, name, Balance) values (3, 'Donald', 3000);